import React, { Component } from "react";
import { PLAY_GAME } from "./redux/contants/xucXacConstant";
import { connect } from "react-redux";

class KetQua extends Component {
  render() {
    return (
      <div className="text-center pt-5 display-4">
        <button onClick={this.props.handlePlayGame} className="btn btn-success">
          <span className="display-4">Play Game</span>
        </button>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      dispatch({
        type: PLAY_GAME,
      });
    },
  };
};
export default connect(null, mapDispatchToProps)(KetQua);
