import { PLAY_GAME } from "./../contants/xucXacConstant";

let initialState = {
  mangXucXac: [
    {
      img: "./img/GameXucXac/1.png",
      giaTri: 1,
    },
    {
      img: "./img/GameXucXac/2.png",
      giaTri: 2,
    },
    {
      img: "./img/GameXucXac/3.png",
      giaTri: 3,
    },
  ],
  luaChon: null,
  tongSoLanChoi: 0,
  tongSoLanThang: 0,
};
export const xucXacReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      console.log("yes");
      // Tạo mảng xúc xắc mới
      let newMangXucXac = state.mangXucXac.map((item) => {
        let random = Math.floor(Math.random() * 6) + 1;
        return {
          img: `./img/GameXucXac/${random}.png`,
          giaTri: random,
        };
      });
      state.mangXucXac = newMangXucXac;
      return { ...state, mangXucXac: newMangXucXac };
    }
    default:
      return state;
  }
};
